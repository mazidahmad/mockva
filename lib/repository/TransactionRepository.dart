import 'package:mockva/model/Transactions.dart';
import 'package:mockva/model/TransferConfirm.dart';
import 'package:mockva/model/TransferInquiry.dart';
import 'package:mockva/provider/TransactionProvider.dart';

class TransactionRepository {
  final TransactionProvider _provider = new TransactionProvider();

  Future<Transactions> getLogs(String accountSrdId) async {
    return await _provider.transactionLog(accountSrdId);
  }

  Future<TransferInquiry> sendTransferInquiry(
      TransferInquiry transferInquiry) async {
    return await _provider.transferInquiry(transferInquiry);
  }

  Future<TransferConfirm> confirm(TransferInquiry transferInquiry) async {
    return await _provider.confirm(transferInquiry);
  }
}
