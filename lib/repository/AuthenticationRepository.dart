import 'package:mockva/model/Auth.dart';
import 'package:mockva/model/Session.dart';
import 'package:mockva/provider/AuthenticationProvider.dart';

class AuthenticationRepository {
  final AuthenticationProvider _provider = new AuthenticationProvider();

  Future<Session> login(Auth auth) async {
    return await _provider.login(auth);
  }

  Future<bool> logout() async {
    return await _provider.logout();
  }
}
