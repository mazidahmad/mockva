import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:mockva/view/AccountPage.dart';
import 'package:mockva/view/HomePage.dart';
import 'package:mockva/view/theme/AppColor.dart';
import 'package:mockva/view/TransferPage.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'HistoryPage.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  PersistentTabController _controller;

  List<Widget> _buildScreens() {
    return [HomePage(), TransferPage(), HistoryPage(), AccountPage()];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: Icon(Ionicons.home),
        inactiveIcon: Icon(Ionicons.home_outline),
        title: ("Home"),
        activeColorPrimary: AppColor.secondary,
        inactiveColorPrimary: AppColor.semiDarkGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Ionicons.swap_vertical),
        title: ("Transfer"),
        activeColorPrimary: AppColor.secondary,
        inactiveColorPrimary: AppColor.semiDarkGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Ionicons.receipt),
        inactiveIcon: Icon(Ionicons.receipt_outline),
        title: ("History"),
        activeColorPrimary: AppColor.secondary,
        inactiveColorPrimary: AppColor.semiDarkGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Ionicons.settings_sharp),
        inactiveIcon: Icon(Ionicons.settings_outline),
        title: ("Account"),
        activeColorPrimary: AppColor.secondary,
        inactiveColorPrimary: AppColor.semiDarkGrey,
      )
    ];
  }

  @override
  void initState() {
    super.initState();
    _controller = PersistentTabController(initialIndex: 0);
  }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      controller: _controller,
      screens: _buildScreens(),
      items: _navBarsItems(),
      confineInSafeArea: true,
      backgroundColor: Colors.white, // Default is Colors.white.
      handleAndroidBackButtonPress: true, // Default is true.
      resizeToAvoidBottomInset:
          true, // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
      stateManagement: true, // Default is true.
      hideNavigationBarWhenKeyboardShows:
          true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
      decoration: NavBarDecoration(
        borderRadius: BorderRadius.circular(10.0),
        colorBehindNavBar: Colors.white,
      ),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      itemAnimationProperties: ItemAnimationProperties(
        // Navigation Bar's items animation properties.
        duration: Duration(milliseconds: 200),
        curve: Curves.ease,
      ),
      screenTransitionAnimation: ScreenTransitionAnimation(
        // Screen transition animation on change of selected tab.
        animateTabTransition: true,
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      navBarStyle:
          NavBarStyle.style3, // Choose the nav bar style with this property.
    );
  }
}
