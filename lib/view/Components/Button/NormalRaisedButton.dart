import 'package:flutter/material.dart';

import 'AbstractButton.dart';

class NormalRaisedButton extends AbstractButton {
  @required
  final double width;
  final Color color;
  final Color iconColor;
  final Color textColor;
  final Color highlightColor;
  final double elevation;
  final double iconSize;
  final double highlightElevation;
  final BorderSide borderSide;
  final EdgeInsets padding;
  final IconData icon;

  NormalRaisedButton(
      {Key key,
      @required void Function() onPressed,
      String text,
      this.width,
      this.color,
      this.textColor,
      this.highlightColor,
      this.elevation,
      this.highlightElevation,
      this.borderSide,
      this.padding,
      this.icon,
      this.iconColor,
      this.iconSize})
      : super(
          key: key,
          onPressed: onPressed,
          text: text,
        );

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: RaisedButton(
        onPressed: onPressed,
        color: color ?? Theme.of(context).colorScheme.primary,
        highlightColor: highlightColor,
        autofocus: true,
        clipBehavior: Clip.antiAlias,
        elevation: elevation ?? 5,
        highlightElevation: highlightElevation,
        padding: padding,
        shape: RoundedRectangleBorder(
          side: borderSide ?? BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(30),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            icon != null
                ? Icon(icon ?? null,
                    color: iconColor ?? null, size: iconSize ?? 0)
                : SizedBox(),
            text != null && icon != null ? SizedBox(width: 20) : SizedBox(),
            text != null
                ? Text(text,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 25))
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
