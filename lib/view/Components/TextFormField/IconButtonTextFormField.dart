import 'package:flutter/material.dart';

import 'AbstractTextFormField.dart';

class IconButtonTextFormField extends AbstractTextFormField {
  final IconData prefixIcon;
  final IconData buttonIcon;
  final Color buttonColor;
  final void Function() buttonOnPressed;
  final double verticalMargin;
  final String suffixText;
  final TextStyle suffixStyle;
  final double width;
  final String filledText;
  final String Function(String) validator;
  final void Function(String) onChanged;

  IconButtonTextFormField(
      {Key key,
      String label,
      String hint,
      TextInputType type,
      bool obscureText,
      TextEditingController controller,
      this.validator,
      this.prefixIcon,
      this.buttonIcon,
      this.buttonColor,
      this.buttonOnPressed,
      this.verticalMargin = 10,
      this.suffixText,
      this.suffixStyle,
      this.width,
      this.filledText,
      this.onChanged})
      : super(
            key: key,
            label: label,
            hint: hint,
            type: type,
            obscureText: obscureText,
            controller: controller);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? MediaQuery.of(context).size.width - 50,
      margin: EdgeInsets.symmetric(vertical: verticalMargin),
      child: ConstrainedBox(
        constraints: new BoxConstraints(
          minHeight: 60,
        ),
        child: Center(
          child: TextFormField(
            autovalidateMode:
                (validator != null) ? AutovalidateMode.onUserInteraction : null,
            onChanged: onChanged ?? null,
            validator: validator ?? null,
            controller: controller,
            keyboardType: type,
            obscureText: obscureText ?? false,
            decoration: InputDecoration(
              prefixIcon: prefixIcon != null ? Icon(prefixIcon) : null,
              suffixIcon: buttonIcon == null
                  ? null
                  : IconButton(
                      onPressed: buttonOnPressed,
                      icon: Icon(
                        buttonIcon,
                        color: buttonColor,
                      ),
                    ),
              suffixText: suffixText,
              suffixStyle: suffixStyle,
              labelText: label,
              border: InputBorder.none,
              contentPadding: EdgeInsets.symmetric(horizontal: 20),
              hintStyle: Theme.of(context).textTheme.bodyText1.copyWith(
                    color: Colors.black45,
                  ),
              hintText: hint,
            ),
            initialValue: filledText,
            style: TextStyle(fontSize: 20),
          ),
        ),
      ),
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.surface,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          new BoxShadow(
            color: Colors.black.withOpacity(0.05),
            blurRadius: 20.0,
          ),
        ],
      ),
    );
  }
}
