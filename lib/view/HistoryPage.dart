import 'package:easy_gradient_text/easy_gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

import 'theme/AppColor.dart';

class HistoryPage extends StatefulWidget {
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: null,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: GradientText(
          text: "HISTORY",
          gradientDirection: GradientDirection.btt,
          colors: [AppColor.primary, AppColor.secondary],
          style:
              TextStyle(fontSize: 30, fontWeight: FontWeight.bold, height: 1),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: List.generate(5, (index) {
              return Container(
                margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                width: MediaQuery.of(context).size.width,
                height: 150,
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    new BoxShadow(
                      color: Colors.black.withOpacity(0.05),
                      blurRadius: 20.0,
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GradientText(
                          text: "TRANSFER",
                          gradientDirection: GradientDirection.btt,
                          colors: [AppColor.primary, AppColor.secondary],
                          style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                              height: 1),
                        ),
                        GradientText(
                          text: "2020-04-03 19:20:00",
                          gradientDirection: GradientDirection.btt,
                          colors: [AppColor.primary, AppColor.secondary],
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              height: 1),
                        ),
                      ],
                    ),
                    Container(
                      height: 70,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Amount : IDR 1.000.000",
                                  style: TextStyle(
                                      height: 1,
                                      color: AppColor.darkGrey,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 20)),
                              Text("Ref : 4518484566521458",
                                  style: TextStyle(
                                      height: 1,
                                      color: AppColor.darkGrey,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 20)),
                              Text("Destination : 84868988122124774",
                                  style: TextStyle(
                                      height: 1,
                                      color: AppColor.darkGrey,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 20)),
                            ],
                          ),
                          Icon(
                            Ionicons.checkmark_circle_outline,
                            size: 50,
                            color: AppColor.secondary,
                          ),
                          SizedBox()
                        ],
                      ),
                    )
                  ],
                ),
              );
            }),
          ),
        ),
      ),
    );
  }
}
