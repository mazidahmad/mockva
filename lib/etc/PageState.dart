enum PageState {
  initialized,
  home,
  history,
  transfer,
  transferInquiry,
  transferDone,
  account,
  login,
  main
}
