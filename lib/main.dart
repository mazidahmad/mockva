import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mockva/view/InitializedPage.dart';
import 'package:mockva/view/LoginPage.dart';
import 'package:mockva/view/MainPage.dart';
import 'package:mockva/view/theme/AppTheme.dart';

import 'etc/PageState.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: AppTheme.light,
      initialRoute: PageState.login.toString(),
      getPages: [
        GetPage(
            name: PageState.initialized.toString(),
            page: () => InitializedPage()),
        GetPage(name: PageState.main.toString(), page: () => MainPage()),
        GetPage(name: PageState.login.toString(), page: () => LoginPage())
      ],
    );
  }
}
