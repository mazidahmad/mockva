import 'package:mockva/model/Session.dart';

class ApiHandler {
  static final ApiHandler _instance = ApiHandler._internal();
  ApiHandler._internal();

  final String _apiUrl = "https://mockva.daksa.co.id/mockva-rest";
  Session session;

  factory ApiHandler() {
    return _instance;
  }
  String get apiUrl => _apiUrl;
}
