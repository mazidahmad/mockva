import 'dart:convert';

class Session {
  String id;
  String accountId;
  DateTime lastLoginTimeStamp;
  String sessionStatus;

  Session({
    this.id,
    this.accountId,
    this.lastLoginTimeStamp,
    this.sessionStatus,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'accountId': accountId,
      'lastLoginTimeStamp': lastLoginTimeStamp.millisecondsSinceEpoch,
      'sessionStatus': sessionStatus,
    };
  }

  factory Session.fromMap(Map<String, dynamic> map) {
    return Session(
      id: map['id'],
      accountId: map['accountId'],
      lastLoginTimeStamp:
          DateTime.fromMillisecondsSinceEpoch(map['lastLoginTimeStamp']),
      sessionStatus: map['sessionStatus'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Session.fromJson(String source) =>
      Session.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Session(id: $id, accountId: $accountId, lastLoginTimeStamp: $lastLoginTimeStamp, sessionStatus: $sessionStatus)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Session &&
        other.id == id &&
        other.accountId == accountId &&
        other.lastLoginTimeStamp == lastLoginTimeStamp &&
        other.sessionStatus == sessionStatus;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        accountId.hashCode ^
        lastLoginTimeStamp.hashCode ^
        sessionStatus.hashCode;
  }
}
