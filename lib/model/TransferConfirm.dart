import 'dart:convert';

class TransferConfirm {
  String accountSrcId;
  String accountDstId;
  int amount;
  DateTime transactionTimeStamp;
  String clientRef;
  TransferConfirm({
    this.accountSrcId,
    this.accountDstId,
    this.amount,
    this.transactionTimeStamp,
    this.clientRef,
  });

  Map<String, dynamic> toMap() {
    return {
      'accountSrcId': accountSrcId,
      'accountDstId': accountDstId,
      'amount': amount,
      'transactionTimeStamp': transactionTimeStamp.millisecondsSinceEpoch,
      'clientRef': clientRef,
    };
  }

  factory TransferConfirm.fromMap(Map<String, dynamic> map) {
    return TransferConfirm(
      accountSrcId: map['accountSrcId'],
      accountDstId: map['accountDstId'],
      amount: map['amount'],
      transactionTimeStamp:
          DateTime.fromMillisecondsSinceEpoch(map['transactionTimeStamp']),
      clientRef: map['clientRef'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransferConfirm.fromJson(String source) =>
      TransferConfirm.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TransferConfirm(accountSrcId: $accountSrcId, accountDstId: $accountDstId, amount: $amount, transactionTimeStamp: $transactionTimeStamp, clientRef: $clientRef)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TransferConfirm &&
        other.accountSrcId == accountSrcId &&
        other.accountDstId == accountDstId &&
        other.amount == amount &&
        other.transactionTimeStamp == transactionTimeStamp &&
        other.clientRef == clientRef;
  }

  @override
  int get hashCode {
    return accountSrcId.hashCode ^
        accountDstId.hashCode ^
        amount.hashCode ^
        transactionTimeStamp.hashCode ^
        clientRef.hashCode;
  }
}
