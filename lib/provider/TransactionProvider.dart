import 'dart:convert';
import 'package:http/http.dart';
import 'package:mockva/model/Transactions.dart';
import 'package:mockva/model/TransferConfirm.dart';
import 'package:mockva/model/TransferInquiry.dart';
import 'package:mockva/util/ApiHandler.dart';

class TransactionProvider {
  Client client = Client();
  final _baseUrl = ApiHandler().apiUrl;

  Future<TransferInquiry> transferInquiry(
      TransferInquiry transferInquiry) async {
    final _url = Uri.parse("$_baseUrl/account/transaction/transferInquiry");
    final response = await client.post(_url,
        headers: {"Content-Type": "application/json"},
        body: json.encode(transferInquiry.toJsonTransfer()));

    final responseJson = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return TransferInquiry.fromJson(responseJson);
    } else {
      return null;
    }
  }

  Future<Transactions> transactionLog(String accountSrcId) async {
    final _url = Uri.parse(
        "$_baseUrl/account/transaction/log?accountSrcId=$accountSrcId");
    final response = await client.get(
      _url,
      headers: {"_sessionId": ApiHandler().session.id},
    );

    final responseJson = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return Transactions.fromJson(responseJson);
    } else {
      return null;
    }
  }

  Future<TransferConfirm> confirm(TransferInquiry transferInquiry) async {
    final _url = Uri.parse("$_baseUrl/transaction/transfer");
    final response = await client.post(_url,
        headers: {
          "Content-Type": "application/json",
          "_sessionId": ApiHandler().session.id
        },
        body: json.encode(transferInquiry.toJsonTransfer()));

    final responseJson = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return TransferConfirm.fromJson(responseJson);
    } else {
      return null;
    }
  }
}
