import 'dart:convert';
import 'package:http/http.dart';
import 'package:mockva/model/Auth.dart';
import 'package:mockva/model/Session.dart';
import 'package:mockva/util/ApiHandler.dart';

class AuthenticationProvider {
  Client client = Client();
  final _baseUrl = ApiHandler().apiUrl;

  Future<Session> login(Auth auth) async {
    final _url = Uri.parse("$_baseUrl/api/auth/login");
    final response = await client.post(_url,
        headers: {"Content-Type": "application/json"},
        body: json.encode(auth.toJson()));

    final responseJson = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return Session.fromJson(responseJson);
    } else {
      return null;
    }
  }

  Future<bool> logout() async {
    final _url = Uri.parse("$_baseUrl/auth/logout");
    final response = await client
        .delete(_url, headers: {"_sessionId": ApiHandler().session.id});

    if (response.statusCode == 200) {
      return true;
    } else {
      return null;
    }
  }
}
